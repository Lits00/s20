let number = prompt("Enter a number:");
console.log("The number you provided is " + number)
for(x = number; x >= 0; x--){
    if(x <= 50){
        console.log(`The current value is at ${x}. Terminating the loop.`)
        break;
    }
    if(x % 10 === 0){
        console.log("The number is divisible by 10. Skipping the number.")
        continue;
    }
    if(x % 5 === 0){
        console.log(x)
        continue;
    }
}

let word = "supercalifragilisticexpialidocious";
let consonants = "";
for(i = 0; i < word.length; i++){
    if(word[i] === 'a' || word[i] === 'e' || word[i] === 'i' || word[i] === 'o' || word[i] === 'u'){ 
        continue;
    }
    else{
        consonants += word[i];  
    }
}
console.log(word)
console.log(consonants)